export const setScorePlayer = (scorePlayer) => {
    return {
        type: 'ADDSCORE',
        scorePlayer: scorePlayer
    }
}

export const resetScorePlayer = (scorePlayer) => {
    return {
        type: 'RESETSCORE',
        scorePlayer: scorePlayer
    }
}