export const setRetryGame = (isRetryGame) => {
    return {
        type: 'TRIGGERSET',
        isRetryGame: isRetryGame
    }
}