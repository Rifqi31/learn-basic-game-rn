const defaultState = {
    isRetryGame: false
}

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'TRIGGERSET':
            return Object.assign({}, state, {
                isRetryGame: action.isRetryGame
            })

        default:
            return state
    }
}