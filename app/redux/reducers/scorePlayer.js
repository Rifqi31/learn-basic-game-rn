const defaultState = {
    scorePlayer: 0
}

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'ADDSCORE':
            let scorePlayer = state.scorePlayer + action.scorePlayer
            return Object.assign({}, state, {
                scorePlayer: scorePlayer
            })
        case 'RESETSCORE':
            let defaultScore = state.scorePlayer - action.scorePlayer
            return Object.assign({}, state, {
                scorePlayer: defaultScore
            })
        default:
            return state
    }
}