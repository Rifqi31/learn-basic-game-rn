import { combineReducers } from 'redux'
import scorePlayer from './scorePlayer'
import player from './player'

const rootReducer = combineReducers({
    scorePlayer,
    player
})

export default rootReducer
