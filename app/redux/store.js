import { applyMiddleware, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'
import rootReducers from './reducers'
import thunk from 'redux-thunk'

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}

const middleWare = []

if (__DEV__) {
    middleWare.push(createLogger)
}

const persistReducers = persistReducer(persistConfig, rootReducers)

export const store = createStore(
    persistReducers,
    undefined,
    composeWithDevTools(applyMiddleware(thunk))
)

export const peristor = persistStore(store)