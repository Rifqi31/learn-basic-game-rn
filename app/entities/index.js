import Matter from 'matter-js'
import Gimo from '../components/common/gimo'
import Floor from '../components/common/floor'
import Ceiling from '../components/common/ceiling'
import Obstacle from '../components/common/obstacle'
import { width, height, heightRatio, widthRatio } from '../utils/dimension'
import { getRandom, topObstacleHeight, bottomObstacleHeight } from '../utils/random'
import Constants from '../utils/constants'

//  -- Overriding this function because the original references HTMLElement
Matter.Common.isElement = () => false

export default restart => {
    // -- Cleanup existing entities
    if (restart) {
        Matter.Engine.clear(restart.physics.engine)
    } 

    let engine = Matter.Engine.create({ enableSleeping: false })
    let world = engine.world
    world.gravity.y = 0.25
    const boxSizeWidth = 32
    const boxSizeHeight = 44

    // Matter.World.add(world, [Gimo, Floor])

    return {
        physics: { engine: engine, world: world},
        Gimo: Gimo(
            world,
            '#ffff',
            // {x: 220, y: 400},
            {x: 100, y: 200},
            {height: boxSizeHeight, width: boxSizeWidth}
        ),
        Floor: Floor(
            world,
            '#ffff',
            {x: width / 2, y: height - 90},
            {height: 100, width: width}
        ),
        Ceiling: Ceiling(
            world,
            '#ffff',
            {x: width / 2, y: 0},
            {height: 50, width: width}
        ),
        Obstacle1: Obstacle(
            world,
            'top',
            // {x: width * 2 - Constants.TOP_PIPE_WIDTH / 2, y: getRandom(100, 150)},
            {x: width * 2 - 500, y: getRandom(100, 400)},
            {height: topObstacleHeight, width: Constants.TOP_PIPE_WIDTH}
        ),
        Obstacle2: Obstacle(
            world,
            'bottom',
            {x: width - Constants.BOTTOM_PIPE_WIDTH / 2, y: getRandom(heightRatio * 300, heightRatio * 300)},
            {height: bottomObstacleHeight, width: Constants.BOTTOM_PIPE_WIDTH}
        ) 
    }
}