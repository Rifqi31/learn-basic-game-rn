const Constants = {
    TOP_PIPE_WIDTH: 70,
    BOTTOM_PIPE_WIDTH: 70,
    collisionCategories : {
        gimo: 0x0001,
        floor: 0x002,
        ceiling: 0x0004,
        obstacle: 0x0008
    }
}

export default Constants
