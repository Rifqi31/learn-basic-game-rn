import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import Modal from 'react-native-modal'

class ModalGame extends React.Component {

    render() {
        const { isVisible, onPressMainMenu, onPressRetry, modalText } = this.props
        return (
            <Modal
                isVisible={isVisible}
                animationIn={"bounceIn"}
                animationOut={"bounceOut"}
                animationInTiming={10}
                animationOutTiming={10}
                style={styles.modal}
            >
                <View style={styles.modalArea}>
                    <View style={styles.contentArea}>
                        <Text style={styles.modalTitle}>{modalText}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ marginRight: 5 }}>
                                <TouchableOpacity style={styles.button} onPress={onPressMainMenu}>
                                    <Text style={styles.buttonText}>Main Menu</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginLeft: 5 }}>
                                <TouchableOpacity style={styles.button} onPress={onPressRetry}>
                                    <Text style={styles.buttonText}>Play Again</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        alignItems: 'center'
    },
    modalArea: {
        backgroundColor: '#ffff',
        borderRadius: 10,
        padding: 10,
        width: wp(70),
        justifyContent: 'center'
    },
    contentArea: {
        alignItems: 'center'
    },
    modalTitle: {
        fontSize: 25, fontWeight: 'bold', paddingBottom: 30
    },
    button: {
        backgroundColor: '#202646',
        borderRadius: 10,
        padding: 20,
        width: wp(30),
        height: wp(15),
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 20, fontWeight: 'bold', color: 'white'
    }
})

export default ModalGame
