import React from 'react'
import { View, Image } from 'react-native'
import { array, object, string } from 'prop-types'
import Matter from 'matter-js'
import { dirt } from './assetData'
import Constants from '../../utils/constants'

const Floor = props => {
    const width = props.size[0]
    const height = props.size[1]
    const x = props.body.position.x - width / 2
    const y = props.body.position.y - height / 2
    return (
        <View style={{
            position: 'absolute',
            left: x,
            top: y,
            width: width,
            height: height,
            backgroundColor: props.color || '#ffff'
        }}>
            <Image
                style={{
                    width: width,
                    height: height
                }}
                source={dirt}
                // resizeMode='repeat'
            />
        </View>
    )
}

export default (world, color, pos, size) => {
    const initialFloor = Matter.Bodies.rectangle(
        pos.x,
        pos.y,
        size.width,
        size.height,
        {
            label: 'floor',
            isStatic: true,
            friction: 1,
            // collisionFilter: {
            //     category: Constants.collisionCategories.floor,
            //     mask: Constants.collisionCategories.gimo
            // }
        }
    )
    Matter.World.add(world, [initialFloor])

    return {
        body: initialFloor,
        size: [size.width, size.height],
        color: color,
        renderer: <Floor />
    }
}

Floor.propTypes = {
    size: array,
    body: object,
    color: string
}