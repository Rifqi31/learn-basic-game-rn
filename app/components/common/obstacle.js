import React from 'react'
import { View } from 'react-native'
import { array, object, string } from 'prop-types'
import Matter from 'matter-js'
import Constants from '../../utils/constants'

const Obstacle = props => {
    const width = props.size[0]
    const height = props.size[1]
    const x = props.body.position.x - width / 2
    const y = props.body.position.y - height / 2
    return (
        <View
            style={{
                position: 'absolute',
                left: x, 
                top: y,
                width: width,
                height: height,
                borderRadius: 20,
                backgroundColor: '#ffff'
            }}
        />
    )
}

export default (world, type, pos, size) => {
    const initialObstacle = Matter.Bodies.rectangle(
        pos.x,
        pos.y,
        size.width,
        size.height,
        {
            label: 'obstacle',
            isStatic: true,
            isSensor: true,
            friction: 1,
            collisionFilter: {
                category: Constants.collisionCategories.obstacle,
                mask: Constants.collisionCategories.gimo
            }
        }
    )
    Matter.World.add(world, [initialObstacle])

    return {
        body: initialObstacle,
        size: [size.width, size.height],
        type: type,
        scored: false,
        renderer: <Obstacle />
    }
}

Obstacle.propTypes = {
    size: array,
    body: object,
    color: string
}