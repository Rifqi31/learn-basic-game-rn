import Sound from 'react-native-sound'

// image asset
export const gimo = require('../../assets/sprites/gimo.png')
export const dirt = require('../../assets/sprites/full_dirt_brown.png')

// sound asset
const jumpSFX = require('../../assets/sounds/jump_sfx.wav')
const gameOverSFX = require('../../assets/sounds/game_over_sfx.wav')
const ouchSFX = require('../../assets/sounds/ouch_sfx.wav')
// Enable playback in silence mode
Sound.setCategory('Playback');
export const soundJump = new Sound(jumpSFX, Sound.MAIN_BUNDLE)
export const soundGameOver = new Sound(gameOverSFX, Sound.MAIN_BUNDLE)
export const soundOuch = new Sound(ouchSFX, Sound.MAIN_BUNDLE)
