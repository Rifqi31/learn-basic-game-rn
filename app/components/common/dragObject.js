import React from 'react'
import { connect } from 'react-redux'
import { setScorePlayer } from '../../redux/actions/scorePlayer'
import { View, StyleSheet, PanResponder, Animated } from 'react-native'
import Sound from 'react-native-sound'

class DragObject extends React.Component {
    constructor(props) {
        super(props)

        Sound.setCategory('Playback', true);

        this.state = {
            isShowDraggable: true,
            dropAreaValues: null,
            posPan: new Animated.ValueXY(),
            opacity: new Animated.Value(1)
        }
    }

    UNSAFE_componentWillMount = () => {
        // Add a listener for the delta value change
        this._val = { x: 0, y: 0 }
        this.state.posPan.addListener((value) => this._val = value)

        // Initiate PanResponder with move handling
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (e, gesture) => true,
            onPanResponderGrant: (e, gesture) => {
                this.state.posPan.setOffset({
                    x: this._val.x,
                    y: this._val.y
                })
                this.state.posPan.setValue({ x: 0, y: 0 })
            },
            onPanResponderMove: Animated.event([
                null, { dx: this.state.posPan.x, dy: this.state.posPan.y }
            ]),
            onPanResponderRelease: (e, gesture) => {
                // Animated.spring(this.state.posPan, {
                //     toValue: { x: 0, y: 0 },
                //     friction: 5
                // }).start()

                // if object through in drop area, object will banish
                if (this._isDropArea(gesture)) {
                    // bansish the object
                    Animated.timing(this.state.opacity, {
                        // animate value
                        toValue: 0,
                        // banish duration
                        // duration: 1000
                        duration: 100
                    }).start(() => {
                        this.setState({
                            isShowDraggable: false
                        }, () => {
                            this.props.onSaveScorePlayer(1)
                            this._playSoundFX()
                        })
                    })
                } else {
                    // if object didn;t to drop area, back to default position
                    Animated.spring(this.state.posPan, {
                        toValue: { x: 0, y: 0 },
                        friction: 5
                    }).start()
                }
            }
        })
        // adjusting delta values
        this.state.posPan.setValue({ x: 0, y: 0 })
    }

    _isDropArea = (gesture) => {
        // size of drop area
        return gesture.moveY < 300
    }

    _playSoundFX = () => {
        const dropCoin = new Sound('retro_accomplished_sfx.wav', Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('failed to load the sound', error)
            } else {
                dropCoin.play()
            }
        })
    }


    render() {
        return (
            <View style={{ width: '20%', alignItems: 'center' }}>
                {this._renderDraggable()}
            </View>
        )
    }

    _renderDraggable = () => {
        const panStyle = { transform: this.state.posPan.getTranslateTransform() }
        const { isShowDraggable } = this.state
        if (isShowDraggable) {
            return (
                <View style={{ position: 'absolute' }}>
                    <Animated.View
                        {...this.panResponder.panHandlers}
                        style={[panStyle, styles.circle, { opacity: this.state.opacity }]}
                    />
                </View>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        scorePlayer: state.scorePlayer.scorePlayer,
        isRetryGame: state.player.isRetryGame
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSaveScorePlayer: (scorePlayer) => {
            dispatch(
                setScorePlayer(scorePlayer)
            )
        }
    }
}

let CIRCLE_RADIUS = 30
let styles = StyleSheet.create({
    circle: {
        backgroundColor: "skyblue",
        width: CIRCLE_RADIUS * 2,
        height: CIRCLE_RADIUS * 2,
        borderRadius: CIRCLE_RADIUS
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(DragObject)
