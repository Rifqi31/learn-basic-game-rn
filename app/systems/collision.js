import Matter from 'matter-js'

const Collision = (entities, {time, dispatch}) => {
    let engine = entities.physics.engine
    Matter.Events.on(engine, 'collisionStart', event => {
        let collision = Matter.SAT.collides(event.pairs[0].bodyA, event.pairs[0].bodyB) // if collide the value are true
        if (collision.collided && collision.bodyA.label == 'gimo' && collision.bodyB.label == 'obstacle' ) {
            // sending signal type game over
            dispatch({ type: 'gameOver' })
        }
    })
    return entities
}

export default Collision
