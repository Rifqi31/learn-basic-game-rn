import Matter from 'matter-js'
// import { soundOuch } from '../components/common/assetData'

const Physics = (entities, {time, dispatch}) => {
    // let engine = entities.physics.engine
    let engine = entities["physics"].engine
    Matter.Engine.update(engine, time.delta)
    return entities
}

export default Physics
