import Physics from './physics'
import Collision from './collision'
import Gimo from './gimo'
import Obstacle from './obstacle'

export default [Physics, Collision, Gimo, Obstacle]
