import Matter from 'matter-js'
import { soundJump } from '../components/common/assetData'

const updateGimo = (entities, {touches, time}) => {
    const engine = entities.physics.engine
    touches.filter(t => t.type === 'press').forEach(t => {
            soundJump.play()
            Matter.Body.setVelocity(entities.Gimo.body, {
                x: entities.Gimo.body.velocity.x,
                y: -5
            })
        })
    Matter.Engine.update(engine, time.delta)
    return entities
}

export default updateGimo
