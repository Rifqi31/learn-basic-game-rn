import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// mainApp
import MainMenu from './mainMenu'
import GamePlayExample1 from './gameplay/example1'
import GamePlayExample2 from './gameplay/example2'

const MainNavigation = createStackNavigator({
    MainMenu: {
        screen: MainMenu
    },
    GamePlayExample1: {
        screen: GamePlayExample1
    },
    GamePlayExample2: {
        screen: GamePlayExample2
    }
})

export default MainMenuNavigator = createAppContainer(MainNavigation)
