import React from 'react'
import MainMenuNavigator from './navigation'


class Application extends React.Component {
    render() {
        return (
            <MainMenuNavigator />
        )
    }
}

export default Application
