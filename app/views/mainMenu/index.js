import React from 'react'
import { connect } from 'react-redux'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { setRetryGame } from '../../redux/actions/player'

class MainMenu extends React.Component {

    _navigateToGame = (type) => {
        if (type == 'example 1') {
            this.props.navigation.navigate('GamePlayExample1')
            this.props.onSetRetryGameStatus(false)
        } else {
            this.props.navigation.navigate('GamePlayExample2')
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} onPress={() => this._navigateToGame('example 1')}>
                    <Text style={styles.buttonText}>Example 1 Using panResponder & Animated</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.button, {marginTop: 20 }]} onPress={() => this._navigateToGame()}>
                    <Text style={styles.buttonText}>Example 2 Using RNGE & Matter js</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isRetryGame: state.player.isRetryGame
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSetRetryGameStatus: (isRetryGame) => {
            dispatch(
                setRetryGame(isRetryGame)
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 15
    },
    button: {
        backgroundColor: 'skyblue',
        padding: 20,
        borderRadius: 10
    },
    buttonText: {
        color: '#00334d',
        fontWeight: 'bold',
        fontSize: 20 
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu)