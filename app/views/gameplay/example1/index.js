import React from 'react'
import { connect } from 'react-redux'
import { View, Text, StyleSheet } from 'react-native'
import { setModalView, resetScorePlayer } from '../../../redux/actions/scorePlayer'
import { setRetryGame } from '../../../redux/actions/player'
import ModalGame from '../../../components/modal/modalGame'
import DragObject from '../../../components/common/dragObject'

class GamePlayExample1 extends React.Component {

    _isActivateModal = (type) => {
        if (type == 'mainMenu') {
            this.props.onResetScorePlayer(this.props.scorePlayer)
            this.props.navigation.navigate('MainMenu')
        } else {
            this.props.onSetRetryGameStatus(true)
            this.props.onResetScorePlayer(this.props.scorePlayer)
        }
    }

    render() {
        // console.log(this.props.isRetryGame)
        return (
            <View style={styles.container}>
                {this._renderModal(this.props.scorePlayer)}
                <View style={styles.dropZone}>
                    <Text style={styles.dropZoneText}>Drop them here!!</Text>
                </View>
                <View style={styles.dragObjectArea}>
                    <Text style={styles.dragObjectText}>SCORES :</Text>
                    <Text style={styles.dragObjectText}>{this.props.scorePlayer}</Text>
                </View>
                <View style={styles.ballContainer} />
                <View style={styles.directRow}>
                    <DragObject />
                    <DragObject />
                    <DragObject />
                    <DragObject />
                    <DragObject />
                    <DragObject />
                </View>
            </View>
        )
    }

    _renderModal = (paramModal) => {
        return (
            <ModalGame
                isVisible={paramModal == 5 ? true : false}
                modalText={'You Win'}
                onPressMainMenu={() => this._isActivateModal('mainMenu')}
                onPressRetry={() => this._isActivateModal()}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        scorePlayer: state.scorePlayer.scorePlayer,
        isModalView: state.scorePlayer.isModalView,
        isRetryGame: state.player.isRetryGame
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSetModalView: (isModalView) => {
            dispatch(
                setModalView(isModalView)
            )
        },
        onResetScorePlayer: (scorePlayer) => {
            dispatch(
                resetScorePlayer(scorePlayer)
            )
        },
        onSetRetryGameStatus: (isRetryGame) => {
            dispatch(
                setRetryGame(isRetryGame)
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    ballContainer: {
        height: 200
    },
    directRow: {
        flexDirection: 'row'
    },
    dropZone: {
        height: 200,
        backgroundColor: '#00334d'
    },
    dropZoneText: {
        marginTop: 25,
        marginLeft: 5,
        marginRight: 5,
        textAlign: 'center',
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold'
    },
    dragObjectArea: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    dragObjectText: {
        fontSize: 25, fontWeight: 'bold'
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(GamePlayExample1)
