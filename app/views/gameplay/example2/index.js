import React from 'react'
import { View, StyleSheet } from 'react-native'
import { GameEngine } from 'react-native-game-engine'
import ModalGame from '../../../components/modal/modalGame'
import Entities from '../../../entities'
import Systems from '../../../systems'
import { soundGameOver } from '../../../components/common/assetData'

class GamePlayExample2 extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isRunning: true,
            isModalActive: false
        }
        this.gameEngine = null
    }

    _restartGame = () => {
        this.gameEngine.swap(Entities())
        this.setState({
            isModalActive: false,
            isRunning: true
        })
    }

    _pauseGame = () => {
        const { isRunning } = this.state
        this.setState({
            isRunning: !isRunning
        })
    }

    _onEvent = (e) => {
        if (e.type === 'gameOver') {
            soundGameOver.play()
            soundGameOver.setVolume(0.5)
            this.setState({
                isRunning: false,
                isModalActive: true
            })
        }
    }

    _isActivateModal = (type) => {
        if (type == 'backToMainMenu') {
            this.setState({
                isModalActive: false
            }, () => {
                this.props.navigation.navigate('MainMenu')
            })
        } else {
            this._restartGame()
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this._renderModal()}
                <GameEngine
                    ref={ref => { this.gameEngine = ref }}
                    style={styles.gameContainer}
                    onEvent={this._onEvent}
                    systems={Systems}
                    entities={Entities()}
                    running={this.state.isRunning}
                />
            </View>
        )
    }

    _renderModal = () => {
        const { isModalActive } = this.state
        return (
            <ModalGame
                isVisible={isModalActive}
                modalText={'Game Over'}
                onPressMainMenu={() => this._isActivateModal('backToMainMenu')}
                onPressRetry={() => this._isActivateModal()}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'skyblue'
    },
    gameContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }
})

export default GamePlayExample2
