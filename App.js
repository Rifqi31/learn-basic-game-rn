import React from 'react'
import { Provider } from 'react-redux';
import { store } from './app/redux/store'
import Application from './app/views'

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Application />
      </Provider>
    )
  }
}

export default App;
